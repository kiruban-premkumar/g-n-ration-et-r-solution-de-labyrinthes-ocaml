1/0 ;;

failwith ;;
invalid_arg ;;

let f x =
    let y=sin x in
         if y>0. then
             log y
         else
             invalid_arg "logarithme non défini" ;;
f 1.5 ;;
f 3.5 ;;