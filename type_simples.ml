(* type int *)

let q = 17/5 and r = 17 mod 5 ;;

(* type float *)

sqrt 12.5 ;;
ceil 12.5 ;;
ceil (-12.5) ;;
floor 12.5 ;;
floor (-12.5);;

float_of_int (12) ;;
int_of_float (-12.123) ;;
int_of_float (12.987) ;;

(* type bool *)

true ;;
false ;;

let x=0 and y = 1 in
             y/x > 1 ;;
let x=0 and y = 1 in
             x<>0 && y/x > 1 ;;

(* type string *)

let ch = "une chaîne";;
ch.[1];;
"une chaîne".[0];;

(* comparaisons *)

2<1 ;;
'Z' < 'a' ;;
"totalement" < "toto" ;;
'a'<>'A' || -1 = 1 ;;