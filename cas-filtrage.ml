let f x = match x with
              | 0 -> 18
              | 1 -> 24
              | y -> y*y ;;

let f x = match x with
              | 0 -> 18
              | 1. -> 24
              | y -> y*y ;;

let f x = match x with
           0 -> 18
         | 1 -> 24
         | x -> x*x ;;

let f x = match x with
           0 -> 18
         | y -> y*y
         | 1 -> 24 ;;

let f x = match x with
           0 -> 18
         | 1 -> 24 ;;

let et a b =
         match a with
             true -> (match b with
                         true -> true
                       | false -> false)
           | false -> false ;;

let et a b =
         match a with
             true -> match b with
                         true -> true
                       | false -> false
           | false -> false ;;

let et a b =
         match a with
             true -> (match b with
                           true -> true
                         | _    -> false)
            | _   -> false ;;

let f x = match x with
           0 -> 18
         | 1 -> 24
         | _ -> y*y ;;

let f x = match x with
           0 -> 18
         | 1 -> 24
         | _ -> x*x ;;

let f x = match x with
              1 | 2 | 3 -> 1
            | 4 | 5 | 6 -> 2
            | _ -> 5 ;;

let signe x = match x with
           0             ->  0
         | y when y>0    ->  1
         | _             ->  -1 ;;

let signe = function
           0             ->  0
         | y when y>0    ->  1
         | _             ->  -1 ;;

let f x = match x with
           0 -> 18
         | 1 -> 24
         | _ -> x*x ;;

let f = function
           0  -> 18
         | 1  -> 24
         | _  -> x*x ;;

let f = function
           0  -> 18
         | 1  -> 24
         | x  -> x*x ;;

let egal_un = function
           1 -> true
         | _ -> false ;;