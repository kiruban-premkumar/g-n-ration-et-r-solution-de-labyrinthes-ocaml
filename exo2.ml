(* 
	Q2.1 
	ocamlc exo2.ml -o exo2
*)
Printf.printf "Bonjour\n";;

(* 
	Q2.2

Printf.printf "Quel est votre nom? ";;
let nom = read_line();;
Printf.printf "Bienvenue %s\n" nom ;;

*)

(* 
	Q2.3
*)

Printf.printf "Quel est votre nom? %!";;
let nom = Scanf.scanf "%s" (fun x -> x);;
Printf.printf "Bienvenue %s\n" nom ;;

