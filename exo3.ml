(* 
    Q3.1 
	ocamlc exo2.ml -o exo2
*)
Printf.printf "Bonjour\n";;

let cube = fun x -> x ** 3.0 ;;

let pi = 4. *. atan 1. ;;

let volume = fun r -> (4. /. 3.) *. pi *. cube r;;

let surface = fun r -> 4. *. pi *. r *. r;;

let surface_et_volume =
	fun r ->
		let s = surface r in
		let v = volume r in
		(s,v);;

let s_et_v = fun r ->
	(4. *. pi *. r *. r, (4. /. 3.)*. pi *. cube r);;

let s,v = s_et_v 1.0;;

Printf.printf "%f %f \n" s v 