(* 
	Kiruban PREMKUMAR
	___ Projet IPF ___
*)

(*

PARTIE 1 - Quelques fonctions sur les listes 

*)

(* 1.1 Créations de listes *)

(* Question 1. *)

(* Cette fonction permet de construire une liste de tous les entiers entre a et b inclus *)
let rec range a b =
	if a > b then 
		[] 
	else 
		a :: range (a+1) b
;;

(* Question 2. *)

(* 
Cette fonction permet de construire une liste contenant tous les couples (i,j) 
à partir de deux entiers m et n tel que 0 <= i < m et  0 <= j < n  
*)
let range2 a b = List.concat ( List.map ( fun e -> List.map (fun e' -> (e,e')) (range 0 b)) (range 0 a )) ;;

(* 1.2 Mélange d'une liste *)

(* Question 3. *)

(*
Cette fonction renvoie une liste correspondant à l privée de son élément d'indice i
*)
let rec remove_nth l i = 
	if i = 0 then (List.tl l) 
	else (List.hd l) :: (remove_nth (List.tl l) (i-1)) ;;

(* Question 4. *)
(*
Cette fonction renvoie le couple (x,r) où x est un élément de l choisi aléatoirement et où r est la liste l privée de x
*)
let extract_random l = 
	let n =  Random.int (List.length l) in ( (List.nth l n), (remove_nth l n) ) ;;

(* Question 5. *)
(*
Cette fonction permet de mélanger une liste
*)
let rec shuffle l = 
	let n = List.length l in
		if n = 0 then [] else let (x, r) = extract_random l in x :: (shuffle r);;

(* 

PARTIE 2 - Représentation de la grille 

*)

module Cell =
	struct
		type t = int * int
		let compare = Pervasives.compare
	end
;;

module CellMap = Map.Make(Cell) ;;
module CellSet = Set.Make(Cell) ;;
type grid = CellSet.t CellMap.t ;;

(* 2.1 Fonctions sur les graphes *)

(* Question 6. *)

(* add_vertex: Cell.t -> grid -> grid
	sur la donnée d'une case v et d'un graphe g, 
	renvoie un nouveau graphe constitué de g 
	auquel on a ajouté le sommet v (sans successeurs)
*)
let add_vertex (v:Cell.t) (g:grid) : grid = CellMap.add v CellSet.empty g ;;

(* Question 7. *)

(*
	add_edge: Cell.t -> Cell.t -> grid -> grid
	-> sur la donnée de deux cases u et v, et d'un graphe g
	-> renvoie un nouveau graphe constitué de g auquel on a ajouté une arête de u à v
*)
let add_edge (u:Cell.t) (v:Cell.t) (g:grid) : grid =
	let ub = try ( CellMap.find u g ) with Not_found -> (CellSet.empty) in
		if (CellSet.mem v ub) then g else CellMap.add u ( CellSet.add v ub) g;;

(* Question 8. *)

(*
	add_edges: Cell.t -> Cell.t -> grid -> grid
	-> sur la donnée de deux cases u et v, et d'un graphe g
	-> renvoie un nouveau graphe constitué de g auquel on a ajouté (si besoin) les arêtes de u à v et de v à u
*)
let add_edges u v g = add_edge v u (add_edge u v g) ;;

(* 2.2 Génération d'une grille *)

(* Question 9. *)

(*
	is_valid: int -> int -> Cell.t -> bool 
	-> is_valid m n c retourne true si c correspond bien à une case de la grille rectangulaire de taille m × n
	-> et false sinon
*)
let is_valid m n (c:Cell.t) = let (x,y) = c in (x >= 0 && x < n) && (y >= 0 && x < m) ;;

(* Question 10. *)

(*
	get_neighbours: Cell.t -> Cell.t list
	-> sur la donnée de c, renvoie la liste constituée des quatre voisins potentiels de c
*)
let get_neighbours (c : Cell.t) : Cell.t list = let (x,y) = c in [ (x, y+1); (x+1, y); (x, y-1); (x-1, y) ] ;;

(* Question 11. *)

(*
	get_valid_neighbours: int -> int -> Cell.t -> Cell.t list
	-> sur la donnée de m, n et c, dresse la liste de tous les voisins de la case c sur une grille m × n
*)
let get_valid_neighbours m n c = List.filter (is_valid m n) (get_neighbours c) ;;

(* Question 12. *)

(* 
	create_grid: int -> int -> grid
	-> sur la donnée de m et n, génère le graphe correspondant à une grille rectangulaire de taille m × n
*)
let create_grid m n =
	let lc = range2 (n-1) (m-1) in
		List.fold_left ( 
			fun acc x -> List.fold_right (add_edges x) (get_valid_neighbours m n x) acc ) ( List.fold_right add_vertex lc CellMap.empty ) lc
	;;

(* 

PARTIE 3 - Affichage et premiers tests 

*)

(* Question 13. *)

(*
	get_center_coord: Cell.t -> int * int
	-> sur la donnée d'une case c, retourne les coordonnées (x, y) du centre de la case
*)
let get_center_coord (c:Cell.t) = let (i1, i2) = c in (2*i1 +1, 2*i2 +1) ;;

(* Question 14. *)

(*
	get_contour: Cell.t -> (int * int) list
	-> sur la donnée d'une case c, retourne la liste des coordonnées des quatre coins de c
*)
let get_contour (c:Cell.t) = 
  let (i1, i2) = c in
  let x = 2*i1 and y = 2*i2 in
    [(x,y); (x,y+2); (x+2,y+2); (x+2,y)]
;;

(* Question 15. *)

(*
	get_wall: Cell.t -> Cell.t -> (int * int) 
	-> Sur la donnée de deux cases c1 et c2, retourne la liste des coordonnées des coins communs à c1 et c2
*)
let get_wall c1 c2 = 
  let l1 = get_contour c1 and l2 = get_contour c2 in
    List.fold_left (fun acc x -> if (List.mem x l2) then [x] @ acc else acc) [] l1
;;

(* Question 16. *)

(*

#use "display.ml" ;;
let f = create_grid 100 100 ;;
test (400,400) (4,5,5) f (3,4) (23,42) [] ;;

*)

(* 

PARTIE 4 - Génération et résolution d'un labyrinthe 

*)

(* Question 17. *)

(* 
	A partir d'une grille g servant de support, un labyrinthe m en cours de construction, un sommet v et une liste l, On construit un nouveau labyrinthe avec au minimum les mêmes murs que m.
*)
let rec generate_maze_aux (g : grid) (m : grid) (v:Cell.t) (l:Cell.t list) = 
  if (List.length l) = 0 then m
  else
    begin
      let v' = List.hd l and t = List.tl l in
      if (CellMap.mem v' m) then m
      else
        begin
          let m' = add_edges v v' (add_vertex v' m) in
          let l' = CellSet.elements (CellMap.find v' g) in
          ( generate_maze_aux g ( generate_maze_aux g m' v' (shuffle l') ) v t )
        end

    end 
;;

(* Question 18. *)

(*
	generate_maze: grid -> grid
	-> Sur la donneé d'une grille, retourne un labyrinthe sur cette grille
*)
let generate_maze (g : grid) =
  	let (v,b) = CellMap.choose g 
	in generate_maze_aux g (CellMap.add v CellSet.empty CellMap.empty) v ( CellSet.elements b )
;;


(* Question 19. *)

(* 
	Fonction recursive pour résoudre le labyrinthe
*)

let rec solve_maze_aux = fun (g : grid) (u : Cell.t) v l s acc ->
  if (u = v || List.mem v l) then acc @ [v]
  else match l with
    | [] -> acc
    | u' :: t ->
      if CellSet.mem u' s then solve_maze_aux g u v t s acc
      else let s' = CellSet.add u' s in
        let l' = CellSet.elements (CellMap.find u' g) in
        let p = solve_maze_aux g u' v l' s' (acc @ [u']) in
        if List.nth p ( (List.length p) - 1 ) = v then p
        else solve_maze_aux g u v t s' acc
;;

(* 
	résout le labyrinthe.
*)
let solve_maze (lab:grid) (e:Cell.t) s =
  let l = CellSet.elements (CellMap.find e lab) in
  let ss = CellSet.singleton e in
  solve_maze_aux lab e s l ss [e]
;;

(* 

PARTIE 5 - Pour aller plus loin 

*)

(* Non traité *)