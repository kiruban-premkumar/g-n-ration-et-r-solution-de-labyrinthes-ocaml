(1,2) ;;
1,2 ;;
let v = 1,2 ;;

let addition (x,y) = x+y ;;
addition (1,2) ;;
addition 1,2 ;;

let addition x,y = x+y ;;

let euclide (x,y) = (x/y , x mod y) ;;
euclide (5,3) ;;

let euclide n =
         let (x,y) = n in
             (x/y , x mod y) ;;

let inverse (a,b) = (b,a) ;;
inverse (1,2) ;;
inverse 1 ;;


(12, "octobre") ;;
let a = (1,1.,true,"un") ;;

let un = (1, "un")
and deux = (2., "deux") ;;

let couple = (un, deux) ;;

let ((a,b),(c,d)) = couple ;;

(a,b,c) ;;

let (x,y,z) = couple ;;

let implique v =
         match v with
               (true, true)   -> true
             | (true, false)  -> false
             | (false, true)  -> true
             | (false, false) -> true ;;

let implique v =
         match v with
               (true, x) -> x
             | (false, x) -> true ;;

let implique v =
         match v with
               (true, x) -> x
             |   _       -> true ;;

let implique = function
               (true, x) -> x
             |  _        -> true ;;

let implique (a,b) =
         match a with
               true -> b
             | _    -> true ;;


(* fonctionnement du filtrage *)

let un = 1 ;;
let est_un = function
           un -> true
         | _  -> false ;;

est_un 2 ;;


let test_à_un x = x = un ;;


let egal = function
           (x,x) -> true
         | _     -> false ;;

let (x,x) = (1,2) ;;

let egal = function
         (x,y) when x=y -> true
         | _ -> false ;;