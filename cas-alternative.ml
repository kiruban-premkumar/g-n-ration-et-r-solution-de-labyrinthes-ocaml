if 2 > 1 then 
   "plus grand"
else 
   "plus petit" ;;

if 2 < 1 then 
   "plus petit"
else 
   "plus grand" ;;

if true then 
   1. 
else 
   0 ;;

2 * (if false then 3 else 5) ;;

let valeur_absolue x =
   if x >= 0 then 
      x
   else 
      -x ;;

let et_if a b = 
   if a then 
      if b then 
         true
      else 
         false
   else 
      if b then 
         false
      else 
         false ;;

let et_if a b = 
   if a then 
      b
   else 
      false ;;