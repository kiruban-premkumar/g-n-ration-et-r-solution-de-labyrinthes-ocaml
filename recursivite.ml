let fact n =
         if n=0 then 1
         else n * fact (n-1) ;;


let rec fact n =
         if n=0 then 1
         else n * fact (n-1) ;;
fact 3 ;;

let rec compte_a_rebours n =
         if n=0 then ()
         else
             begin
                 print_int n ; print_string " ";
                 compte_a_rebours (n-1)
             end ;;

compte_a_rebours 5 ;;

(* attention : récursivité infinie ! *)
let rec compte_a_rebours n =
         begin
             print_int n ; print_string " " ;
             compte_a_rebours (n-1)
         end ;;

let rec compte n =
         if n=0 then ()
         else
           begin
             compte (n-1) ;
             print_int n ; print_string " "
           end ;;

compte 5 ;;

let rec fact n =
         if n=0 then 1
         else n * fact (n-1) ;;

let rec f n =
         if n=0 then 1
         else f (n-1) ;;

let rec fibo = function
           0 | 1 -> 1                        (* 0 "ou" 1 *)
         | n     -> fibo(n-1) + fibo(n-2) ;;

let rec acker = function
           (0,n) -> n+1
         | (m,0) -> acker (m-1,1)
         | (m,n) -> acker (m-1, acker (m,n-1)) ;;

let rec pair = function
                     0 -> true
                   | n -> impair (n-1)
       and   impair = function
                     0 -> false
                   | n -> pair (n-1) ;;

pair 5 ;;
impair 5 ;;

let rec somme n =
         if n=0 then 0
         else n + somme (n-1) ;;


let rec fibo = function
           0 | 1 -> 1
         | n     -> fibo(n-1) + fibo(n-2) ;;

(* calcule le nombre d'appel à fibo pour un n donné *)
let rec r = function
         0 | 1   -> 1
         | n     -> r (n-1) + r (n-2) + 1 ;;

r 4 ;;
r 5 ;;
r 6 ;;
r 10 ;;
r 20 ;;
r 30 ;;