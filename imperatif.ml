print_string "Hello!";;

print_string ;;

print_char 'A' ;;

print_int 1 ;;

print_float (log 5.) ;;

print_endline "toto" ;;

print_string "Hello " ; print_string "world !\n";;

1+2 ;
0 ;;

print_string "Voilà ";
print_string "Caml !";
print_newline () ;;

let affiche n = 
  if n mod 2 = 0 then
    print_int n ;;

let affiche n = 
  if n mod 2 = 0 then
    n ;;

affiche 3 ;;

let affiche n = 
  if n mod 2 = 0 then
    print_int n ;
    print_string " est pair" ;;

affiche 3 ;;

let affiche n = 
  if n mod 2 = 0 then
    begin
      print_int n ;
      print_string " est pair" 
    end ;;

affiche 3 ;;