(* EXO 6.1 *)


let rec puiss =
	fun x -> fun n ->
		if n = 0 
		then 1
		else x * puiss x (n-1);;

let puiss = 
	let rec aux = fun x n acc ->
		if n = 0 then acc
	else aux x (n-1) (x*acc)
in
fun x n -> aux x n 1;;