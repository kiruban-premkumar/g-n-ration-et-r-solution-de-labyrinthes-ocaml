(* EXO 5 *)

let deux_elts = fun l ->
	match l with
	| x::y::[] -> true
	| _ -> false;;

deux_elts [1,2];;

let third = fun t ->
	match t with
	| _::_::z::[] -> z
	| _ -> assert false;;

third [1;2;3;4];;

let third = fun t ->
	match t with
	| (_,_,z) -> z;;

(1,'2',true);;