(* 
    Q4.1
*)

let affiche_pair = fun n ->
	if n mod 2 = 0 then
		Printf.printf "pair"
	else
		Printf.printf "impair";;

let affiche_pair = fun n ->
	Printf.printf "%s\n"
	(if n mod 2 then "pair"
				else "impair");;

(* 
    Q4.2
*)

let pair = fun n ->
	if n mod 2 = 0
		then true
		else false;;


let pair = fun n ->
	n mod 2 = 0;;


(* 
    Q4.3
*)

let max = func a,b ->
	if a > b then a else b;;

let max_3 = fun a, b, c ->
	max(max(a,b),c);;